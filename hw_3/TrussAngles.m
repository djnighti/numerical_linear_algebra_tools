function [angle_mat] = TrussAngles(g, e, neighbors)
% g: geomtry vector
% e: element connections to nodes
% neighbors: node connections to elements (nodes c,d,e,f,g) (occupancy map)
len_e = length(e);
theta = zeros(1,len_e);
for n =1:len_e
    theta(1,n) = rad2deg(atan2(g(e(n,2),2)-g(e(n,1),2),g(e(n,2),1)-g(e(n,1),1)));
end

for n = 1:length(g)/2
len_theta =  length(theta);
c = zeros(1,len_theta);
s = zeros(1,len_theta);
angle_mat = zeros(len_theta);
end

for n = 1:len_theta
    c(1,n)=cosd(theta(n));
    s(1,n)=sind(theta(n));
end

for n = 1:len_theta/2
    angle_mat(2*n-1,:) = c(1,:).*neighbors(n,:);
    angle_mat(2*n,:) = s(1,:).*neighbors(n,:);
end
angle_mat;
end


function [f, answer_validity] = TrussForces(g, e, neighbors,z)
[angle_mat] = TrussAngles(g, e, neighbors);
s = svd(angle_mat);
s_max = max(max(s));
s_min = min(min(s));
condition_number = s_max/s_min;
[B, angle_mat_mod] = Gauss(angle_mat);
[f] = GaussLU(angle_mat_mod, z);
if condition_number < 1E4
    answer_validity = "valid answers";
else
    answer_validity = "NOT valid answers";
end


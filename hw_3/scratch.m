function [f, answer_validity] = TrussForces(g, e, neighbors)
% Loads and reaction forces for each node
%Fa = [F1  0   0   0 0  0  0   0   0  0]';
%Fb = [0   F2  F3 0  0  0  0   0   0  0]';
%Fc = [F1  F2  0  F4 F5 0  0   0   0  0]';
%Fd = [0   0   F3 F4 0  F6 F7  0   0  0]';
%Fe = [0   0   0  0  F5 F6 0   F8  F9 0]';
%Ff = [0   0   0  0  0  0  F7  F8  0  F10]';
%Fg = [0   0   0  0  0  0  0   0   F9 F10]';

% fcx = [F1x F2x 0   F4x F5x 0   0   0   0   0]';
% fcy = [F1y F2y 0   F4y F5y 0   0   0   0   0]';
% fdx = [0   0   F3x F4x 0   F6x F7x 0   0   0]';
% fdy = [0   0   F3y F4y 0   F6y F7y 0   0   0]';
% fex = [0   0   0   0   F5x F6x 0   F8x F9x 0]';
% fey = [0   0   0   0   F5y F6y 0   F8y F9y 0]';
% ffx = [0   0   0   0   0   0   F7x F8x 0   F10x]';
% ffy = [0   0   0   0   0   0   F7y F8y 0   F10y]';
% 0   = [0   0   0   0   0   0   0   0   F9x F10x]';
% W   = [0   0   0   0   0   0   0   0   F9y F10y]';

% angles
len_e = length(e);
theta = zeros(1,len_e);
for n =1:len_e
%     n
%     node = [e(n,1) e(n,2)]
%     gy = [g(e(n,1),2) g(e(n,2),2)]
%     gx = [g(e(n,1),1) g(e(n,2),1)]
%     gy_diff = g(e(n,2),2)-g(e(n,1),2)
%     gx_diff = g(e(n,2),1)-g(e(n,1),1)
    theta(1,n) = rad2deg(atan2(g(e(n,2),2)-g(e(n,1),2),g(e(n,2),1)-g(e(n,1),1)));
end


for n = 1:length(g)/2
len_theta =  length(theta);
c = zeros(1,len_theta);
s = zeros(1,len_theta);
angle_mat = zeros(len_theta);
end

for n = 1:len_theta
    c(1,n)=cosd(theta(n));
    s(1,n)=sind(theta(n));
end

for n = 1:len_theta/2
    angle_mat(2*n-1,:) = c(1,:).*neighbors(n,:);
    angle_mat(2*n,:) = s(1,:).*neighbors(n,:);
end
angle_mat;
  % angle2= [c(1) c(2) 0    c(4) c(5) 0    0    0    0    0;
%          s(1) s(2) 0    s(4) s(5) 0    0    0    0    0;
%          0    0    c(3) c(4) 0    c(6) c(7) 0    0    0;
%          0    0    s(3) s(4) 0    s(6) s(7) 0    0    0;
%          0    0    0    0    c(5) c(6) 0    c(8) c(9) 0;
%          0    0    0    0    s(5) s(6) 0    s(8) s(9) 0;
%          0    0    0    0    0    0    c(7) c(8) 0    c(10);
%          0    0    0    0    0    0    s(7) s(8) 0    s(10);
%          0    0    0    0    0    0    0    0    c(9) c(10);
%          0    0    0    0    0    0    0    0    s(9) s(10)];
% B = [0 135 180 90 0 135 180 90 45 180]
w = 1000;
b = [0 0 0 0 0 0 0 0 0 w]';
s = svd(angle_mat);
s_max = max(max(s));
s_min = min(min(s));
condition_number = s_max/s_min;
[B, angle_mat_mod] = Gauss(angle_mat);
[f] = GaussLU(angle_mat_mod, b);
if condition_number < 1E4
    answer_validity = "valid answers";
else
    answer_validity = "NOT valid answers";
end




%% Clean Slate
clc; clear all; close all;
%#ok<


% geomtry vector
n1 = [0 0];
n2 = [1 0];
n3 = [2 0];
n4 = [3 1];
n5 = [2 1];
n6 = [1 1];
n7 = [0 1];
g = [n1 ;n2 ;n3 ;n4 ;n5 ;n6 ;n7];

% element connections to nodes (10 elements, 7 nodes)
e =[[1 2]; [2 7]; [6 7]; [2 6]; [2 3]; [3 6]; [5 6]; [3 5]; [3 4]; [4 5]];

% node connections to elements (nodes c,d,e,f,g) (occupancy map)
neighbors = [[1,1,0,1,1,0,0,0,0,0];
             [0,0,1,1,0,1,1,0,0,0];
             [0,0,0,0,1,1,0,1,1,0];
             [0,0,0,0,0,0,1,1,0,1];
             [0,0,0,0,0,0,0,0,1,1]];

% Calculate forces
[f, answer_validity] = TrussForces(g,e, neighbors)

%% Problem 4.2

% Assign density assumption to elements in compression and tension
for n = 1:length(f)
   if f(n) > 0 % compression
       p(n) = 5; % mass/length
   else % tension
       p(n) = 0; % negligible mass  
   end
end


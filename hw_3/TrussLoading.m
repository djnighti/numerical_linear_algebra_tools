function [xe,fe] = TrussLoading(g, e, neighbors,z)
[K,D,C] = TrussK(g, e, neighbors)
[B, K_mod] = Gauss(K);
[xe] = -1*GaussLU(K_mod, z);
fe = -C*D*xe;
end


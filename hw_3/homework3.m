%% Clean Slate
clc; clear all; close all;
%#ok<*NOPTS>

%% Homework 3
% Dominic Nightingale
% MAE 207 NLA
% Professor Bewley

%% Homework 3: Problem 4.1 Static loading of the structure
% Problem setup

% geomtry vector
n1 = [0 0];
n2 = [1 0];
n3 = [2 0];
n4 = [3 1];
n5 = [2 1];
n6 = [1 1];
n7 = [0 1];
g = [n1 ;n2 ;n3 ;n4 ;n5 ;n6 ;n7];

% element connections to nodes (10 elements, 7 nodes)
e =[[1 2]; [2 7]; [6 7]; [2 6]; [2 3]; [3 6]; [5 6]; [3 5]; [3 4]; [4 5]];
num_nodes = max(max(e));
num_elements = length(e);

% node connections to elements (nodes c,d,e,f,g) (occupancy map)
neighbors = [[1,1,0,1,1,0,0,0,0,0];
             [0,0,1,1,0,1,1,0,0,0];
             [0,0,0,0,1,1,0,1,1,0];
             [0,0,0,0,0,0,1,1,0,1];
             [0,0,0,0,0,0,0,0,1,1]];

%% TrussForces
% Calculate forces
w = 1000;
z = [0 0 0 0 0 0 0 0 0 w]';
[f, answer_validity] = TrussForces(g, e, neighbors, z)

%% Homework 3: Problem 4.2 Displacement of the structure under load

%% TrussK & TrussLoading

[K,D,C] = TrussK(g, e, neighbors)
[L] = TrussDistance(g, e)
% Assign density assumption to elements in compression and tension
for n = 1:num_elements
   if f(n) > 0 % compression
       p(n) = 5; % mass/length
   else % tension
       p(n) = 0; % negligible mass  
   end
end
m0 = 0.1;
m1 = zeros(1,10);
m2 = zeros(1,10);
m3 = zeros(1,10);
m4 = zeros(1,10);
m1(:) = repelem(p(1:2:10)'.*L(1:2:10),2);
m2(:) = repelem(p(2:2:10)'.*L(2:2:10),2);
m3(1:8) = repelem(p(4:2:10)'.*L(4:2:10),2);
m4(1:6) = repelem(p(5:2:10)'.*L(5:2:10),2);
m = m0*num_nodes + 0.5*(m1+m2+m3+m4)
a = 9.8;
z = zeros(10,1);
z(2:2:10) = a*m(2:2:10)'
z(10) = z(10) + w;
[xe,fe] = TrussLoading(g, e, neighbors, z)

% plot displacements
Xe = [0 0 xe(1:2:end)'];
Ye = [0 0 xe(2:2:end)']; 

s = 500; % scaling 
count2 = 1;
X = g(:,1);
Y = g(:,2);
% for loop to reorder the x and y coords to be able to plot the truss
for q = 1:10
i=e(q,1);
j=e(q,2);
xnew(count2) = X(i);
xnew(count2 + 1) = X(j);
ynew(count2) = Y(i);
ynew(count2+1) = Y(j);
x_disp(count2) = X(i)+s*Xe(i);
x_disp(count2+1) = X(j)+s*Xe(j);
y_disp(count2) = Y(i)+s*Ye(i);
y_disp(count2+1) = Y(j)+s*Ye(j);
count2 = count2+2; 
end

figure
hold on;
plot(xnew,ynew,'b')
plot(x_disp,y_disp,'r')
axis([0 4 -1 2])
N_text = ['Deformation Scale = ', num2str(s)];
legend('Undeformed',N_text)
xlabel('X [m]')
ylabel('Y [m]')
title('Truss Deformation: Configuration 1')

%% Problem 4.3 Vibration modes of the structure

M = diag(m);
T = sqrt(M);
A = (T^-1)*K*(T^-1)';
[V,D] = eig(A); 
w = sqrt(D); 
w = diag(w);
yk = (T^-1)*V;

% plot five modes with lowest frequencies 
figure
for i = 1:5
   [x_disp,y_disp] = Reorder(yk,i,e,X,Y); 
   subplot(5,1,i)
   plot(x_disp,y_disp)
   axis([0 3.25 -0.5 1.5])
   Omega = "%.2f rad/s";
   Omega = compose(Omega,w(i));
   xlabel('\omega=' + Omega)
end
subplot(5,1,1)
title('Five Modes of Truss Structure: Configuration 1')

function [x_disp,y_disp] = Reorder(yk,col,e,X,Y)
xprime = yk(:,col)';
Xprime = [0 0 xprime(1:2:end)];
Yprime = [0 0 xprime(2:2:end)]; 
count2 = 1;
for q = 1:10
    i=e(q,1);
    j=e(q,2);
    x_disp(count2) = X(i)+Xprime(i);
    x_disp(count2+1) = X(j)+Xprime(j);
    y_disp(count2) = Y(i)+Yprime(i);
    y_disp(count2+1) = Y(j)+Yprime(j);
    count2 = count2+2; 
end
end
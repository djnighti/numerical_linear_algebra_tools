function [K,D,C] = TrussK(g, e, neighbors)
[D] = TrussAngles(g, e, neighbors);
[L] = TrussDistance(g, e);
C =10^7./L; % get spring constant matrix
C = diag(C);
K = D'*C*D;
end


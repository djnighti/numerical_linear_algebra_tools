function [L] = TrussDistance(g,e)
x1 = g(e(:,1),1);
y1 = g(e(:,1),2);
x2 = g(e(:,2),1);
y2 = g(e(:,2),2);
L = sqrt((x2-x1).^2 + (y2-y1).^2); % get distance between coordinate points
end


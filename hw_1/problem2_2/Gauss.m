function [B ,A] = Gauss(A)
% Solve AX=B
n = length(A);
B = eye(n);
for j = 1: n-1 % FORWARD SWEEP
A(j + 1: n , j) = - A(j + 1: n , j) / A(j , j);
A(j + 1: n , j + 1: n) = A(j + 1: n , j + 1: n) + A(j + 1: n , j) * A(j , j + 1: n); % ( Outer product update )
B(j + 1: n , :) = B(j + 1: n , :) + A( j + 1: n , j) * B( j , :) ;
end
for i = n : -1: 1 % BACK SUBSTITUTION
B(i , :) = (B( i , :) - A(i , i + 1: n) * B (i + 1: n , :)) / A(i , i) ; % ( Inner product update )
end
end 



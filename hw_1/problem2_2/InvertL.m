function [L_inv] = InvertL(L)
n = length(L);
L_inv = eye(n);
for j = 1:n
    L_inv(j,1:j) = (L_inv(j,1:j) - L(j,1:j-1) * L_inv(1:j-1,1:j))/L(j,j);
end
end


function [B] = GaussLU(Amod, B)
n = length(Amod);
for j = 2: n
B(j , :) = B(j , :) + Amod(j , 1 : j -1) * B (1 : j - 1 , :) ; % FORWARD SUBSTITUTION
end
for i = n : -1: 1 ,
B (i , :) = (B(i , :) - Amod (i , i + 1: n) * B (i + 1: n , :)) / Amod(i , i); % BACK SUBSTITUTION
end
end


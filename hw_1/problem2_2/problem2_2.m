%% Clean Slate
clc; clear all; close all;

%% Homework 1: Problem 2.2
% Dominic Nightingale
% MAE 207 Numerical Linear Algebra
% Professor Bewley

%% Problem Statement
% Generalizing slightly the first few lines of Algorithm 2.2, 
% note that, if L is lower triangular,
% LX = B may be solved via forward substitution as follows:
%
% for j = 1:n
% B(j,:) = (B(j,:) - L(j,1:j-1) * B(1:j-1,:))/L(j,j);
% end
%
% Initializing with B = I, as in §2.1, thus provides an algorithm to 
% compute the inverse of L. In this special
% case with B = I, this algorithm may be streamlined such that the 
% computation may be done in place in the
% lower-triangular part of the L matrix (that is, without creating a 
% separate B matrix). Write this streamlined
% algorithm as a Matlab function InvertL.m, as well as a test script 
% InvertLTest.m, in a style similar to the other codes presented in this chapter.
% Run this algorithm on a randomly-generated lower triangular matrix L;
% does Fact 2.1 hold in your numerical experiment?

%% Testing
num_tests = 100;
test_vec = zeros(1, length(num_tests));
passed_count = 0;
mat_size = 3;
for n=1:num_tests
    L = tril(rand(mat_size))
    [L_inv] = InvertL(L)
    is_nan_result = round(sum(sum(isnan(L_inv))));
    is_inf_result = round(sum(sum(isinf(L_inv))));
    if is_nan_result==0 && is_inf_result==0
        check_I = round(sum(sum(L * L_inv - eye(mat_size))));
        if check_I==0
            test_vec(n) = 1;
            passed_count=passed_count + 1;
        end
    else
        test_vec(n) = 1;
        passed_count=passed_count + 1;
    end
end

%% Analysis- does Fact 2.1 hold in your numerical experiment?
% Fact 2.1 
% The inverse of a unit lower triangular matrix is unit lower 
% triangular. The inverse of a lower triangular matrix T is lower  
% triangular, with the diagonal elements of T −1 being the inverse of the 
% diagonal elements of T. Analogous statements hold for the corresponding 
% upper and block forms.
% End Fact 2.1 
%
% From inspection, yes the inverse of the diagonal elements in L are the
% diagonal elements in L inverse.


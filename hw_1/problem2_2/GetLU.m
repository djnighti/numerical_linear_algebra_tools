function [L, U] = GetLU(Amod)
n = length(Amod);
B = eye(n);
for j = 1:n
L(j,:) = (B(j,:));
end

for j = 2: n
L(j , :) = B(j , :) - Amod(j , 1 : j -1) * B (1 : j - 1 , :) ; % FORWARD SUBSTITUTION
end

for i = n : -1: 1 ,
U(i , :) = (B(i , :) + Amod (i , i + 1: n) * B (i + 1: n , :)) / Amod(i , i); % BACK SUBSTITUTION
end
end


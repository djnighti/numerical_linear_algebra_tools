function [R] = CirculantHankel(top)
% one vectors: 'top'
% defined such that
% top = [1 2 3 4 5]
% to result in (after this function call):
%      1     2     3     4     5
%      2     3     4     5     1
%      3     4     5     1     2
%      4     5     1     2     3
%      5     1     2     3     4
n= length(top); 
for row = 1: n
    R(row , :) = [top(row : end) top(1 : row-1)]; 
end
end


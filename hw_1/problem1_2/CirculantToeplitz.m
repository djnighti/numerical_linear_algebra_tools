function [R] = CirculantToeplitz(top)
% one vectors: 'top'
% defined such that
% top = [1 2 3 4 5]
% to result in (after this function call):
%      1     2     3     4     5
%      5     1     2     3     4
%      4     5     1     2     3
%      3     4     5     1     2
%      2     3     4     5     1
n= length(top); 
for row = 1: n
    R(row , :) = [top(end-row+2 : end) top(1 : end-row+1)]; 
end
end


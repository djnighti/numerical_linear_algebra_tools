%% Clean Slate
clc; clear all; close all;

%% Homework 1: Problem 2.2
% Dominic Nightingale
% MAE 207 Numerical Linear Algebra
% Professor Bewley

%% Problem Statement
% construct the following types of matricies:
%   -toeplitz (top row and left column)
%   -circulant Hankel (top row only)
%   -circulant Toeplitz (top row only)

%% Toeplitz
top = [1 2 3 4 5];
left = [1 6 7 8 9];
T_mat = Toeplitz(top , left)

%% circulant Toeplitz

CT_mat = CirculantToeplitz(top)

%% Hankel
right = [5 6 7 8 9];
H_mat = Hankel(top , right)

%% circulant Hankel
CH_mat = CirculantHankel(top)

function R=Hankel(top , right)
% two vectors: 'top' and 'right'
% defined such that
% top = [1 2 3 4 5]
% right = [6 7 8 9 10]
% to result in (after this function call):
%      1     2     3     4     5
%      2     3     4     5     7
%      3     4     5     7     8
%      4     5     7     8     9
%      5     7     8     9    10
n= length(top); 
for row = 1: n
    R(row , :) = [top(row : n) right(2 : row)];
end
end


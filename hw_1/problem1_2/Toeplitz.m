function [R] = Toeplitz(top , left)
% two vectors: 'top' and 'right'
% defined such that
% top = [1 2 3 4 5]
% left = [6 7 8 9 10]
% to result in (after this function call):
%      1     2     3     4     5
%      6     1     2     3     4
%      7     6     1     2     3
%      8     7     6     1     2
%      9     8     7     6     1
n= length(top); 
for row = 1: n
    R(row , :) = [flip(left(2 : row)) top(1 : n-row+1)]; 
end
end

